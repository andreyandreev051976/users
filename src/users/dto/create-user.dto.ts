import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    description: 'Name of user',
    default: 'John',
  })
  name: string;
  @ApiProperty({
    description: 'Name of department',
    default: 'Development',
  })
  department: string;
  @ApiProperty({
    description: 'Name of company',
    default: 'My company',
  })
  company: string;
  @ApiProperty({
    description: 'Job title',
    default: 'Frontend developer',
  })
  jobTitle: string;
}
