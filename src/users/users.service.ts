import { Injectable, NotFoundException } from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersRepository } from './users.repository';
import { CreateUserDto } from './dto/create-user.dto';
import { Pagination } from 'src/shared/pagination/pagination.decorator';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersRepository)
    private readonly usersRepository: UsersRepository,
  ) {}
  async create(createUserDTO: CreateUserDto) {
    const user = this.usersRepository.create(createUserDTO);
    await this.usersRepository.insert(user);
    return user;
  }

  async findAll({ limit, offset, page, size }: Pagination) {
    const [users, total] = await this.usersRepository.findAndCount({
      take: limit,
      skip: offset,
      order: {
        id: 'ASC',
      },
    });
    return {
      total,
      items: users,
      page,
      size,
    };
  }

  async findOne(id: number) {
    const user = await this.usersRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.usersRepository.update({ id }, updateUserDto);
    return await this.findOne(id);
  }

  async remove(id: number) {
    await this.findOne(id);
    return this.usersRepository.delete({ id });
  }
}
