import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import setupConfiguration from './setup/configuration/configuration';
import { DatabaseModule } from './setup/database/database.module';
import { VariablesModule } from './setup/variables/variables.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    ConfigModule.forRoot(setupConfiguration()),
    VariablesModule,
    DatabaseModule,
    UsersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
