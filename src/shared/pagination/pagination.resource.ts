export class PaginatedResource<T> {
  total: number;
  items: T[];
  page: number;
  size: number;
}
